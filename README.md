## How to set up:

### Do you already have PHP 8.1 or higher?
###
#### Set your site to parse in
```config/config.php```
####
#### Install dependencies(psr-4 autoload)
```composer i```
####
#### Run
```make run```
#### or
```php index.php```

# This task was done with the help of ChatGPT. It assisted me in achieving accurate parsing