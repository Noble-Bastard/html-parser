<?php

use app\Gateway\HTMLParserGateway;
use app\Services\HTMLParserService;
use app\Services\TagCounterService;

$config = require('config/config.php');

require_once('vendor/autoload.php');


$tagCounter = new TagCounterService();
$parserGateway = new HTMLParserGateway($config['urlToParse'], $tagCounter);

$parserService = new HTMLParserService($parserGateway);

foreach ($parserService->parse() as $tags) {
    echo $tags;
}