<?php

namespace app\Contracts;

interface HTMLParserServiceInterface
{
    public function parse();
}