<?php

namespace app\Contracts;

interface TagCounterInterface
{
    public function countTags($html);
}