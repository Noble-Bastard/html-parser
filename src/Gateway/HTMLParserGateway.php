<?php

namespace app\Gateway;

use app\Contracts\TagCounterInterface;

class HTMLParserGateway
{

    public function __construct(
        private readonly string $url,
        private readonly TagCounterInterface $tagCounter
    ) {
    }

    public function parse()
    {
        $html = $this->fetchHTML();
        return $this->tagCounter->countTags($html);
    }

    private function fetchHTML(): bool|string
    {
        return file_get_contents($this->url);
    }
}