<?php

namespace app\Services;

use app\Contracts\HTMLParserServiceInterface;
use app\Gateway\HTMLParserGateway;
use Generator;

class HTMLParserService implements HTMLParserServiceInterface
{
    public function __construct(
        private readonly HTMLParserGateway $htmlParserGateway
    ) {
    }

    public function parse(): Generator
    {
        $tagCounts = $this->htmlParserGateway->parse();

        foreach ($tagCounts as $tag => $count) {
            yield $tag . ': ' . $count . PHP_EOL;
        }
    }
}
