<?php

namespace app\Services;

use app\Contracts\TagCounterInterface;

class TagCounterService implements TagCounterInterface
{
    public function countTags($html): array
    {
        $tagCounts = array();

        preg_match_all('/<([a-zA-Z0-9]+)[^>]*>/', $html, $matches);
        $tags = $matches[1];

        foreach ($tags as $tag) {
            if (isset($tagCounts[$tag])) {
                $tagCounts[$tag]++;
            } else {
                $tagCounts[$tag] = 1;
            }
        }

        return $tagCounts;
    }
}